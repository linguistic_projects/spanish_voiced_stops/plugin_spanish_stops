# Copyright 2017 Rolando Muñoz

dir$ = "../../preferences/index.Table"

if fileReadable(dir$)
    tb = Read from file: dir$
else
    exitScript: "The Index Table has not been found"
endif
