# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

@config.init: "../../preferences/logfile.txt"

fileName$ = chooseReadFile$: "Sept 1: Open a tab separated file"
if fileName$ <> ""
  tb = Read Table from tab-separated file: fileName$
else
  exitScript()
endif

if Object_'tb'.ncol < 1
  exitScript: "This table must have 1 column at least"
endif

beginPause: "step 2"
  comment: "Match the column names"
  optionMenu: "Key column", 1
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

selectObject: tb
Save as text file: "../../preferences/info_speakers.Table"
@config.setField: "speaker_table.key",  Object_'tb'.col$[key_column]
removeObject: tb
