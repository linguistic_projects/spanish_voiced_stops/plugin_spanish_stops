# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc
include ../../procedures/get_row_where.proc
include ../../procedures/append_row.proc
include ../../procedures/list_recursive_path.proc

@config.init: "../../preferences/logfile.txt"

## targets_table
targetTableDir$ = "../../preferences/info_targets.Table"
tb_key_value$ = config.init.return$["targets_table.key"]
tb_phoneme$ = config.init.return$["targets_table.phoneme"]
tb_word$ = config.init.return$["targets_table.word"]
tb_phrase_code$ = config.init.return$["targets_table.code"]
tb_word_occurence$ = config.init.return$["targets_table.word.occurrence"]
tb_phoneme_occurence$ = config.init.return$["targets_table.phoneme.occurrence"]

## tg
tgFolderDir$ = config.init.return$["tg_folder.dir"]
tier_phoneme$ = config.init.return$["tg_tier.phoneme"]
tier_word$ = config.init.return$["tg_tier.word"]
tier_code$ = config.init.return$["tg_tier.code"]

##
recursive_search = number(config.init.return$["recursive_search"])

#
tb_index = Create Table with column names: "index", 0, "filename tier speaker key_value text tmin tmax tmid status file_path notes"
tb_index_missing = Create Table with column names: "index_missing", 0, "error_message key_value text file"
tb_targets = Read from file: targetTableDir$

# List all the files in the root directory
if recursive_search
  @findFiles: tgFolderDir$, "*.TextGrid"
  fileList = findFiles.return
else
  fileList = Create Strings as file list: "fileList", tgFolderDir$ + "/*.TextGrid"
endif
selectObject: fileList
nFiles = Get number of strings

for iFile to nFiles
  tgName$ = object$[fileList, iFile]
  tgDir$ = tgFolderDir$ + "/" + tgName$
  # Open tgs
  tg = Read from file: tgDir$
  baseName$ = selected$("TextGrid")
  # Create tables
  tb_tg = Down to Table: "no", 16, "yes", "no"
  for iRow to object[tb_targets].nrow
    key_value = object[tb_targets, iRow, tb_key_value$]
    phrase_code$ = object$[tb_targets, iRow, tb_phrase_code$]
    word$ = object$[tb_targets, iRow, tb_word$]
    word_occurence = object[tb_targets, iRow, tb_word_occurence$]
    phoneme$ = object$[tb_targets, iRow, tb_phoneme$]
    phoneme_occurence = object[tb_targets, iRow, tb_phoneme_occurence$]
    selectObject: tb_tg
    tb_code = nowarn Extract rows where: "self$[""tier""]== tier_code$ and self$[""text""] = phrase_code$"
    for code_row to object[tb_code].nrow
      code_tmin = object[tb_code, code_row, "tmin"]
      code_tmax = object[tb_code, code_row, "tmax"]
      selectObject: tb_tg
      tb_word = nowarn Extract rows where: "self$[""tier""] == tier_word$ and self[""tmin""] >= code_tmin and self[""tmax""] <= code_tmax"
      if object[tb_word].nrow = 0
        selectObject: tb_index_missing
        @appendRow
        @append$: "error_message", "Word not found"
        @append: "key_value", key_value
        @append$: "text", word$
        @append$: "file", tgName$
        selectObject: tb_word
      endif
      @getRowWhere_regex: "text", word$, word_occurence
      if getRowWhere_regex.return
        word_row = getRowWhere_regex.return
        word_tmin = object[tb_word, word_row, "tmin"]
        word_tmax = object[tb_word, word_row, "tmax"]
        selectObject: tb_tg
        tb_phoneme = nowarn Extract rows where: "self$[""tier""] == tier_phoneme$ and self[""tmin""] >= word_tmin and self[""tmax""] <= word_tmax"
        if object[tb_phoneme].nrow = 0
          selectObject: tb_index_missing
          @appendRow
          @append$: "error_message", "Phone not found"
          @append: "key_value", key_value
          @append$: "text", phoneme$
          @append$: "file", tgName$
          selectObject: tb_phoneme
        endif
        @getRowWhere_regex: "text", phoneme$, phoneme_occurence
        if getRowWhere_regex.return
          phoneme_row= getRowWhere_regex.return	
          phoneme_text$ = object$[tb_phoneme, phoneme_row, "text"]
          phoneme_tmin = object[tb_phoneme, phoneme_row, "tmin"]
          phoneme_tmax = object[tb_phoneme, phoneme_row, "tmax"]
          selectObject: tb_index
          @appendRow
          @append$: "filename", baseName$
          @append$: "file_path", tgName$
          @append$: "tier", tier_phoneme$
          @append: "key_value", key_value
          @append$: "text", phoneme_text$
          @append: "tmin", phoneme_tmin
          @append: "tmax", phoneme_tmax
        endif
        removeObject: tb_phoneme
      endif
      removeObject: tb_word
    endfor
    removeObject: tb_code
  endfor
  removeObject: tg, tb_tg
endfor
removeObject: tb_targets, fileList
selectObject: tb_index

Formula: "tmid", "(self[ row, ""tmin""] + self[ row, ""tmax""])*0.5"
Formula: "speaker", "left$(self$[""filename""], 3)"
Formula: "status", "0"
