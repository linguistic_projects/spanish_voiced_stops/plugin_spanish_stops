# Copyright 2017 Rolando Muñoz

Add menu command: "Objects", "Goodies", "Spanish stops", "", 0, ""

# Index
Add menu command: "Objects", "Goodies", "Scan TextGrids...", "Spanish stops", 1, "scripts/index/index_by_phones_form.praat"

## Index > Import table
Add menu command: "Objects", "Goodies", "Import", "Spanish stops", 1, ""
Add menu command: "Objects", "Goodies", "Index table...", "Import", 2, "scripts/settings/index_table_import.praat"
Add menu command: "Objects", "Goodies", "Targets table...", "Import", 2, "scripts/settings/target_table_import.praat"
Add menu command: "Objects", "Goodies", "Speakers table...", "Import", 2, "scripts/settings/speaker_table_import.praat"

## Index > Export table
Add menu command: "Objects", "Goodies", "Export", "Spanish stops", 1, ""
Add menu command: "Objects", "Goodies", "Index table", "Export", 2, "scripts/settings/index_table_export.praat"
Add menu command: "Objects", "Goodies", "Targets table", "Export", 2, "scripts/settings/target_table_export.praat"
Add menu command: "Objects", "Goodies", "Speakers table", "Export", 2, "scripts/settings/speaker_table_export.praat"

# Preferences
Add menu command: "Objects", "Goodies", "Preferences", "Spanish stops", 1, ""
Add menu command: "Objects", "Goodies", "Set TextGrid tiers...", "Preferences", 2, "scripts/settings/settings_tg.praat"
Add menu command: "Objects", "Goodies", "-", "Spanish stops", 1, ""
Add menu command: "Objects", "Goodies", "About", "Spanish stops", 1, "./scripts/about.praat"
