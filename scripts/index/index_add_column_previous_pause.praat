# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

## index_table
tb_index = selected("Table")
Append column: "previous_pause"
Formula: "previous_pause", "0"

@config.init: "../../preferences/logfile.txt"
tgFolderDir$ = config.init.return$["tg_folder.dir"]
tierPhoneme$ = config.init.return$["tg_tier.phoneme"]

for i to Object_'tb_index'.nrow
  tgName$ = Object_'tb_index'$[i, "file_path"]
  tgName_tmp$ = Object_'tb_index'$[i-1, "file_path"]
  tmin = Object_'tb_index'[i, "tmin"]
  
  if tgName$ != tgName_tmp$
    nocheck removeObject: tg
    tgDir$ = tgFolderDir$ + "/" + tgName$
    tg = Read from file: tgDir$
    nTiers = Get number of tiers
    for j to nTiers
      tierName$ = Get tier name: j
      if tierName$ == tierPhoneme$ 
        tierPhoneme = j
        j = nTiers
      else
        tierPhoneme = 0
      endif
    endfor
  endif

  selectObject: tg
  low_interval = Get low interval at time: tierPhoneme, tmin
  interval_text$ = Get label of interval: tierPhoneme, low_interval
  if interval_text$ == ""
    selectObject: tb_index
    Set numeric value: i, "previous_pause", 1  
  endif
endfor
nocheck removeObject: tg 

selectObject: tb_index
