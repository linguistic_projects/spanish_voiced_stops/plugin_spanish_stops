# Copyright 2017 Rolando Muñoz

dir$ = "../../preferences/info_speakers.Table"

if fileReadable(dir$)
    tb = Read from file: dir$
else
    exitScript: "The Speaker table has not been found"
endif
