# Copyright 2017 Rolando Muñoz

include ../../procedures/config.proc

@config.init: "../../preferences/logfile.txt"

fileName$ = chooseReadFile$: "Sept 1: Open a tab separated file"
if fileName$ <> ""
  tb = Read Table from tab-separated file: fileName$
else
  exitScript()
endif

if Object_'tb'.ncol < 2
  exitScript: "This table must have 2 columns at least"
endif

beginPause: "step 2"
  comment: "Match the column names"
  optionMenu: "Target key column", 1
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
  optionMenu: "Speaker key column", 2
  for i to Object_'tb'.ncol
    option: Object_'tb'.col$[i]
  endfor
clicked = endPause: "Continue", "Quit", 1

tb_tmp = Create Table without column names: "table", 0, 2
col# = {target_key_column, speaker_key_column}
for i to 2
  col = col# [i]
  column_index = Get column index: Object_'tb'.col$[col]
  if column_index
    exitScript: "A duplicate column """ + Object_'tb'.col$[col] + """ has been found"
  endif
  Set column label (index): i, Object_'tb'.col$[col]
endfor

if clicked = 2
  exitScript()
endif

selectObject: tb
Save as text file: "../../preferences/info_phrases.Table"
@config.setField: "index_table.key2target", Object_'tb'.col$[target_key_column]
@config.setField: "index_table.key2speaker", Object_'tb'.col$[speaker_key_column]

removeObject: tb, tb_tmp

pauseScript: "Completed successfully"
