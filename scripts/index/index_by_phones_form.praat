# Copyright 2017 Rolando Muñoz Aramburú

include ../../procedures/config.proc

@config.init: "../../preferences/logfile.txt"

beginPause: "index"
  comment: "Set the folder directories where files are located"
  sentence: "TextGrid folder", config.init.return$["tg_folder.dir"]
  sentence: "Audio folder", config.init.return$["sd_folder.dir"]
  boolean: "Recursive search", 0
  comment: "Add columns to index:"
  boolean: "Rich report", number(config.init.return$["recursive_search"])
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "sd_folder.dir", audio_folder$
@config.setField: "tg_folder.dir", textGrid_folder$
@config.setField: "recursive_search", string$(recursive_search)

runScript: "index_by_phones.praat"

if rich_report
  runScript: "index_add_column_previous_pause.praat"
  runScript: "index_add_column_intensity_report.praat"
  runScript: "index_join2tables.praat"
else
  Append column: "intensity_min"
  Append column: "intensity_max"
  Formula: "intensity_min", "50"
  Formula: "intensity_max", "100"
endif
Save as text file: "../../preferences/index.Table"
tb = selected("Table")
finder_case = if Object_'tb'.nrow > 0 then 1 else 0 fi
@config.setField: "finder_case", string$(finder_case)

pauseScript: "Completed successfully"
